import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';

class EditProfile extends StatefulWidget {
  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Profile"),
        centerTitle: true,
        backgroundColor: Color(0xFF1B418D),
        automaticallyImplyLeading: true,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text("Full Name"),
                    Row(
//                      mainAxisSize: MainAxisSize.min,
                      children: [
                        ChoiceChip(
                          padding: EdgeInsets.zero,
//                              labelPadding: EdgeInsets.symmetric(
//                                  vertical: 1, horizontal: 3.0),
                          label: Text(
                            "Mr.",
                            style: TextStyle(color: Colors.white),
                          ),
                          //backgroundColor: Color(0xFF1B418D),
                          selected: true,
                          selectedColor: Color(0xFF1B418D),
                          shape: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(20.0),
                                topLeft: Radius.circular(20.0),
                              )),
                        ),
                        ChoiceChip(
                          padding: EdgeInsets.zero,

//                              labelPadding: EdgeInsets.symmetric(
//                                  vertical: 1, horizontal: 3.0),
                          label: Text(
                            "Ms.",
                            style: TextStyle(color: Colors.black),
                          ),
                          selected: false,
                          disabledColor: Colors.white,
                          shape: OutlineInputBorder(
                              borderSide: BorderSide.none,
                              borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(20.0),
                                topRight: Radius.circular(20.0),
                              )),
                        ),
                        Expanded(
                            child: TextField(
                          decoration: InputDecoration(border: InputBorder.none),
                        )),
//                          SizedBox(
//                            width: 8.0,
//                          )
                      ],
                    ),
                    Divider(
                      color: Colors.grey,
                      thickness: 1.0,
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Email Address"),
                    TextField(
                      decoration: InputDecoration(),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Phone Number"),
                    TextField(
                      cursorHeight: 20.0,
                      decoration: InputDecoration(),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 15.0, horizontal: 15.0),
                color: Colors.white,
                //color: Colors.red,
                child: RaisedButton(
                  elevation: 0,
                  focusNode: FocusNode(),
                  focusColor: Colors.red,
                  //  color: Colors.red,
                  autofocus: true,
                  onPressed: () {},
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 15.0),
                    child: Text(
                      "Update Now",
                      style: TextStyle(fontSize: 15.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
