import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

class NewRequest extends StatelessWidget {
  final Color textFieldBorderColor = Colors.red[500];

  validate() {}
  final Map<String, TextInputType> keyBoardType = {
    "jobtitle": TextInputType.text,
    "description": TextInputType.text,
    "date": TextInputType.datetime,
    "location": TextInputType.streetAddress,
    "rate": TextInputType.numberWithOptions(decimal: true)
  };

  Widget buildTextFormField(
      int maxLines, Color textFieldBorderColor, String labelText) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 5.0),
      child: Container(
        child: TextFormField(
          keyboardType: keyBoardType[labelText.toLowerCase().trim()],
          maxLines: maxLines,
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(20),
            border: OutlineInputBorder(),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: textFieldBorderColor),
              borderRadius: BorderRadius.circular(8.0),
            ),
            disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: textFieldBorderColor),
              borderRadius: BorderRadius.circular(8.0),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: textFieldBorderColor),
              borderRadius: BorderRadius.circular(8.0),
            ),
            labelText: labelText,
            labelStyle: TextStyle(
              color: textFieldBorderColor,
              fontSize: 15.0,
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('New Request'),
        backgroundColor: Colors.red[500],
        actions: [
          FlatButton(
              onPressed: () {},
              child: Text(
                "Post",
                style: TextStyle(color: Colors.white),
              ))
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              buildTextFormField(1, textFieldBorderColor, "Job Title"),
              buildTextFormField(3, textFieldBorderColor, "Description"),
              buildTextFormField(1, textFieldBorderColor, "Location"),
              buildTextFormField(1, textFieldBorderColor, "Date"),
              buildTextFormField(1, textFieldBorderColor, "Rate"),
//              Padding(
//                padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 5.0),
//                child: Container(
//                  child: TextFormField(
//                    maxLines: 3,
//                    decoration: InputDecoration(
//                      contentPadding: const EdgeInsets.all(20),
//                      border: OutlineInputBorder(),
//                      enabledBorder: OutlineInputBorder(
//                        borderSide: BorderSide(color: textFieldBorderColor),
//                        borderRadius: BorderRadius.circular(8.0),
//                      ),
//                      focusedBorder: OutlineInputBorder(
//                        borderSide: BorderSide(color: textFieldBorderColor),
//                        borderRadius: BorderRadius.circular(8.0),
//                      ),
//                      labelText: 'Description',
//                      labelStyle: TextStyle(
//                        color: textFieldBorderColor,
//                        fontSize: 15.0,
//                      ),
//                    ),
//                  ),
//                ),
//              ),
//              Padding(
//                padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 5.0),
//                child: Container(
//                  height: 50.0,
//                  child: TextFormField(
//                    decoration: InputDecoration(
//                      contentPadding: const EdgeInsets.all(20),
//                      border: OutlineInputBorder(),
//                      enabledBorder: OutlineInputBorder(
//                        borderSide: BorderSide(color: textFieldBorderColor),
//                        borderRadius: BorderRadius.circular(8.0),
//                      ),
//                      focusedBorder: OutlineInputBorder(
//                        borderSide: BorderSide(color: textFieldBorderColor),
//                        borderRadius: BorderRadius.circular(8.0),
//                      ),
//                      labelText: 'Location',
//                      labelStyle: TextStyle(
//                        color: textFieldBorderColor,
//                        fontSize: 15.0,
//                      ),
//                    ),
//                  ),
//                ),
//              ),

//              Padding(
//                padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 5.0),
//                child: Container(
//                  height: 50.0,
//                  child: TextFormField(
//                    decoration: InputDecoration(
//                      contentPadding: const EdgeInsets.all(20),
//                      border: OutlineInputBorder(),
//                      enabledBorder: OutlineInputBorder(
//                        borderSide: BorderSide(color: textFieldBorderColor),
//                        borderRadius: BorderRadius.circular(8.0),
//                      ),
//                      focusedBorder: OutlineInputBorder(
//                        borderSide: BorderSide(color: textFieldBorderColor),
//                        borderRadius: BorderRadius.circular(8.0),
//                      ),
//                      labelText: 'Date',
//                      labelStyle: TextStyle(
//                        color: textFieldBorderColor,
//                        fontSize: 15.0,
//                      ),
//                    ),
//                  ),
//                ),
//              ),
//              Padding(
//                padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 5.0),
//                child: Container(
//                  height: 50.0,
//                  child: TextFormField(
//                    decoration: InputDecoration(
//                      contentPadding: const EdgeInsets.all(20),
//                      border: OutlineInputBorder(),
//                      enabledBorder: OutlineInputBorder(
//                        borderSide: BorderSide(color: textFieldBorderColor),
//                        borderRadius: BorderRadius.circular(8.0),
//                      ),
//                      focusedBorder: OutlineInputBorder(
//                        borderSide: BorderSide(color: textFieldBorderColor),
//                        borderRadius: BorderRadius.circular(8.0),
//                      ),
//                      labelText: 'Rate',
//                      labelStyle: TextStyle(
//                        color: textFieldBorderColor,
//                        fontSize: 15.0,
//                      ),
//                    ),
//                  ),
//                ),
//              ),
            ],
          ),
        ),
      ),
    );
  }
}
