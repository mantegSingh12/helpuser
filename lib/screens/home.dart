import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Home extends StatelessWidget {
  final GlobalKey<ScaffoldState> _drawerScaffoldKey =
      GlobalKey<ScaffoldState>();

  final String logOutRoute = "/";
  final String myPostRoute = "/myPosts";
  final String profileRoute = "/profile";
  final String newRequestRoute = "/newRequest";
  final String editProfileRoute = "/editProfile";
  final String notificationsRoute = "/notifications";
  final String settingsRoute = "/settings";
  final String helperRoute = "/helper";

  final String userName = " James Kershner";

  Widget drawerTab({IconData icon, String title, String routeName}) {
    return Container(
      height: 55.0,
      margin: EdgeInsets.only(left: 12.0, right: 12.0, bottom: 8.0),
      child: RaisedButton(
        disabledColor: Colors.red[500],
        color: Colors.red[500],
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
          side: BorderSide(
            color: Colors.red[500],
          ),
        ),
        onPressed: routeName == null
            ? null
            : routeName == logOutRoute
                ? () {
                    logOut();
                  }
                : () {
                    navigationHandler(routeName);
                  },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              icon,
              color: Colors.white,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 15.0,
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
      decoration: BoxDecoration(
        // color: Colors.red[500],
        borderRadius: BorderRadius.circular(18.0),
        //border: Border.all(color: Colors.red[500])
      ),
    );
  }

  void navigationHandler(String routeName) {
    Navigator.pushNamed(_drawerScaffoldKey.currentState.context, routeName);
  }

  void logOut() {
    Navigator.pushNamedAndRemoveUntil(
        _drawerScaffoldKey.currentState.context, logOutRoute, (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Home'),
        leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {
            if (_drawerScaffoldKey.currentState.isDrawerOpen)
              Navigator.pop(context);
            else
              _drawerScaffoldKey.currentState.openDrawer();
          },
        ),
        backgroundColor: Colors.red[500],
      ),
//      drawer: ,
      body: Scaffold(
        resizeToAvoidBottomInset: false,
        key: _drawerScaffoldKey,
        drawer: Drawer(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: CircleAvatar(
                          backgroundImage: AssetImage(
                            'images/OIP.jpg',
                          ),
                          radius: 30.0,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          userName,
                          style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                drawerTab(
                  icon: Icons.home,
                  title: "Home",
                ),
                drawerTab(
                    icon: Icons.account_circle_rounded,
                    title: "Profile",
                    routeName: profileRoute),
//              Container(
//                height: 55.0,
//                margin: EdgeInsets.only(left: 12.0, right: 12.0, top: 8.0),
//                child: RaisedButton(
//                  color: Colors.red[500],
//                  shape: RoundedRectangleBorder(
//                    borderRadius: BorderRadius.circular(18.0),
//                    side: BorderSide(
//                      color: Colors.red[500],
//                    ),
//                  ),
//                  onPressed: () {},
//                  child: Row(
//                    crossAxisAlignment: CrossAxisAlignment.center,
//                    children: [
//                      Icon(
//                        Icons.home,
//                        color: Colors.white,
//                      ),
//                      Padding(
//                        padding: const EdgeInsets.all(8.0),
//                        child: Text(
//                          'Profile',
//                          style: TextStyle(
//                            fontSize: 15.0,
//                            color: Colors.white,
//                          ),
//                        ),
//                      )
//                    ],
//                  ),
//                ),
//                decoration: BoxDecoration(
//                  // color: Colors.red[500],
//                  borderRadius: BorderRadius.circular(18.0),
//                  //border: Border.all(color: Colors.red[500])
//                ),
//              ),

                drawerTab(
                    icon: FontAwesomeIcons.tools,
                    title: "My Posts",
                    routeName: myPostRoute),
                drawerTab(
                    icon: Icons.notifications,
                    title: "Notifications",
                    routeName: notificationsRoute),
                drawerTab(
                    icon: Icons.settings,
                    title: "Settings",
                    routeName: settingsRoute),
                drawerTab(
                  icon: Icons.logout,
                  title: "Log out",
                  routeName: logOutRoute,
                ),

                Expanded(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, helperRoute);
                        },
                        child: Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Icon(
                                Icons.account_circle_rounded,
                                color: Colors.red[500],
                                size: 30.0,
                              ),
                              SizedBox(
                                width: 10.0,
                              ),
                              Text(
                                'Switch to helper account',
                                style: TextStyle(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red[500],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
