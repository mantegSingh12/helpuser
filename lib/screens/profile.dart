import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Profile extends StatelessWidget {
  Widget tile({IconData icon, String tapTitle}) {
    return InkWell(
      onTap: () {},
      child: Container(
//      color: Colors.yellow,
        padding: EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
        margin: EdgeInsets.only(left: 15.0, top: 10.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: Icon(
                icon,
                size: 25.0,
              ),
            ),
            SizedBox(
              width: 15.0,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  tapTitle,
                  style: TextStyle(fontSize: 15.0),
                ),
                SizedBox(
                  height: 20,
                ),
                SizedBox(
                  child: Container(
                    height: 1,
                    width: 336.4,
                    color: Colors.grey,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("My Profile"),
        elevation: 0.0,
        backgroundColor: Colors.red,
        automaticallyImplyLeading: false,
      ),
      body: Container(
        child: Column(
          children: [
            BuildUserTile(),
            SizedBox(
              width: double.infinity,
              height: 10,
              child: Container(
                color: Colors.grey[200],
              ),
            ),
            Expanded(
              child: Container(
                height: 400,
                child: Column(
                  children: [
                    tile(
                        icon: Icons.help_center_outlined,
                        tapTitle: "Help Center"),
//
                    tile(
                        icon: FontAwesomeIcons.solidHandshake,
                        tapTitle: "Register as Helper"),
                    tile(
                        icon: FontAwesomeIcons.hireAHelper,
                        tapTitle: "About Helper Company"),
                    tile(icon: Icons.share, tapTitle: "Share Helper Company"),
                    tile(
                        icon: Icons.wallet_giftcard, tapTitle: "My Gift Cards"),
                    tile(
                        icon: Icons.account_balance_wallet,
                        tapTitle: "My Wallet"),
                    tile(
                        icon: Icons.star_rate, tapTitle: "Rate Helper Company"),
                    tile(
                        icon: Icons.payment_outlined,
                        tapTitle: "Payment Options"),
                  ],
                ),
                decoration: BoxDecoration(
                    border: Border(
                        top: BorderSide(
                  color: Colors.grey[300],
                ))),
              ),
            ),
            RawChip(
              label: Text("1.0.0"),
            ),
          ],
        ),
      ),
    );
  }
}

class BuildUserTile extends StatefulWidget {
  @override
  _BuildUserTileState createState() => _BuildUserTileState();
}

class _BuildUserTileState extends State<BuildUserTile> {
  final String _userName = "User";
  final String _userEmail = "User@gmail.com";
  final num _number = 0123456789;

  final String editProfileRoute = "/editProfile";
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 15.0),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
//                      mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  _userName,
                  style: TextStyle(
                    fontSize: 18.0,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 6.0),
                  child: Text(
                    _userEmail,
                    style: TextStyle(color: Colors.grey[700], fontSize: 15.0),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 6.0),
                  child: Text(
                    "+91 ${_number.toString()}",
                    style: TextStyle(color: Colors.grey[700], fontSize: 15.0),
                  ),
                )
              ],
            ),
          ),
          Container(
            alignment: Alignment.centerRight,
            child: Center(
              child: IconButton(
                  icon: Icon(Icons.edit_outlined),
                  onPressed: () {
                    Navigator.of(context).pushNamed(editProfileRoute);
                  }),
            ),
          ),
        ],
      ),
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
        color: Colors.grey[300],
      ))),
    );
  }
}
