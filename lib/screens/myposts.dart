import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class MyPosts extends StatefulWidget {
  @override
  _Screen4State createState() => _Screen4State();
}

class _Screen4State extends State<MyPosts> {
  String _userName = "James Kershner";
  String postTime = "3 Hours ago";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('My Posts'),
        backgroundColor: Colors.red,
      ),
      body: Card(
        margin: EdgeInsets.all(15.0),
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              textBaseline: TextBaseline.alphabetic,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 12.0),
                  child: CircleAvatar(
                    backgroundImage: AssetImage('images/OIP.jpg'),
                  ),
                ),
                Text(_userName,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                    )),
                SizedBox(
                  width: 15.0,
                ),
                Text(
                  postTime,
                  style: TextStyle(
                    fontSize: 12.0,
                    color: Colors.red[300],
                  ),
                ),
                SizedBox(
                  width: 15.0,
                ),
                Expanded(
                  child: IconButton(
                    alignment: Alignment.centerRight,
                    icon: Icon(
                      Icons.more_horiz,
                    ),
                    color: Colors.red,
                    onPressed: () {},
                  ),
                ),
              ],
            ),
            Card(
              color: Color(0xfffdf1cb),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  //crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          'Title:',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: Colors.red,
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          'Distance:',
                          textAlign: TextAlign.start,
                          textScaleFactor: 0.9,
                          style: TextStyle(
                            color: Colors.red,
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          'Rate:',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: Colors.red,
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          'Date:',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: Colors.red,
                            fontSize: 18.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          'Garden Cleanup',
                          textAlign: TextAlign.start,
                          //textDirection: TextDirection.ltr,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                        Text(
                          '3 km away',
                          //textDirection: TextDirection.ltr,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            // fontWeight: FontWeight.w900,
                          ),
                        ),
                        Text(
                          '\$ 45.00',
                          // textDirection: TextDirection.ltr,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            // fontWeight: FontWeight.w900,
                          ),
                        ),
                        Text(
                          'August 4, 2018',
                          // textDirection: TextDirection.ltr,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 18.0,
                            // fontWeight: FontWeight.w900,
                          ),
                        ),
                      ],
                    ),
//                      Row(
//                        crossAxisAlignment: CrossAxisAlignment.center,
//                        mainAxisAlignment: MainAxisAlignment.start,
//                        children: <Widget>[
//                          SizedBox(
//                            width: 54.0,
//                            height: 25.0,
//                          ),
//
//                          SizedBox(
//                            width: 10.0,
//                          ),
//
//                        ],
//                      ),
//                      Row(
//                        crossAxisAlignment: CrossAxisAlignment.center,
//                        mainAxisAlignment: MainAxisAlignment.start,
//                        children: <Widget>[
//                          SizedBox(
//                            width: 20.0,
//                          ),
//
//                          SizedBox(
//                            width: 10.0,
//                          ),
//
//                        ],
//                      ),
//                      Row(
//                        crossAxisAlignment: CrossAxisAlignment.center,
//                        mainAxisAlignment: MainAxisAlignment.start,
//                        children: <Widget>[
//                          SizedBox(
//                            width: 54.0,
//                          ),
//
//                          SizedBox(
//                            width: 10.0,
//                          ),
//
//                        ],
//                      ),
//                      Row(
//                        crossAxisAlignment: CrossAxisAlignment.center,
//                        mainAxisAlignment: MainAxisAlignment.start,
//                        children: <Widget>[
//                          SizedBox(
//                            width: 54.0,
//                          ),
//
//                          SizedBox(
//                            width: 10.0,
//                          ),
//
//                        ],
//                      ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
//        Column(
//          children: <Widget>[
//
//            Padding(
//              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
//              child: Container(
//                  height: 100.0,
//                  child: Card(
//                    color: Color(0xfffdf1cb),
//                    shape: RoundedRectangleBorder(
//                      borderRadius: BorderRadius.circular(10.0),
//                    ),
//                    child: Row(
//                      children: <Widget>[
//                        Column(
//                          children: [],
//                        ),
//                        Column(
//                          children: [],
//                        ),
//                        Row(
//                          crossAxisAlignment: CrossAxisAlignment.center,
//                          mainAxisAlignment: MainAxisAlignment.start,
//                          children: <Widget>[
//                            SizedBox(
//                              width: 54.0,
//                              height: 25.0,
//                            ),
//                            Text(
//                              'Title:',
//                              textAlign: TextAlign.start,
//                              style: TextStyle(
//                                color: Colors.red,
//                                fontSize: 18.0,
//                                fontWeight: FontWeight.bold,
//                              ),
//                            ),
//                            SizedBox(
//                              width: 10.0,
//                            ),
//                            Text(
//                              'Garden Cleanup',
//                              textDirection: TextDirection.ltr,
//                              style: TextStyle(
//                                color: Colors.black,
//                                fontSize: 15.0,
//                                fontWeight: FontWeight.w900,
//                              ),
//                            ),
//                          ],
//                        ),
//                        Row(
//                          crossAxisAlignment: CrossAxisAlignment.center,
//                          mainAxisAlignment: MainAxisAlignment.start,
//                          children: <Widget>[
//                            SizedBox(
//                              width: 20.0,
//                            ),
//                            Text(
//                              'Distance:',
//                              textAlign: TextAlign.start,
//                              style: TextStyle(
//                                color: Colors.red,
//                                fontSize: 18.0,
//                                fontWeight: FontWeight.bold,
//                              ),
//                            ),
//                            SizedBox(
//                              width: 10.0,
//                            ),
//                            Text(
//                              '3 km away',
//                              textDirection: TextDirection.ltr,
//                              style: TextStyle(
//                                color: Colors.black,
//                                fontSize: 15.0,
//                                // fontWeight: FontWeight.w900,
//                              ),
//                            ),
//                          ],
//                        ),
//                        Row(
//                          crossAxisAlignment: CrossAxisAlignment.center,
//                          mainAxisAlignment: MainAxisAlignment.start,
//                          children: <Widget>[
//                            SizedBox(
//                              width: 54.0,
//                            ),
//                            Text(
//                              'Rate:',
//                              textAlign: TextAlign.start,
//                              style: TextStyle(
//                                color: Colors.red,
//                                fontSize: 18.0,
//                                fontWeight: FontWeight.bold,
//                              ),
//                            ),
//                            SizedBox(
//                              width: 10.0,
//                            ),
//                            Text(
//                              '\$ 45.00',
//                              textDirection: TextDirection.ltr,
//                              style: TextStyle(
//                                color: Colors.black,
//                                fontSize: 15.0,
//                                // fontWeight: FontWeight.w900,
//                              ),
//                            ),
//                          ],
//                        ),
//                        Row(
//                          crossAxisAlignment: CrossAxisAlignment.center,
//                          mainAxisAlignment: MainAxisAlignment.start,
//                          children: <Widget>[
//                            SizedBox(
//                              width: 54.0,
//                            ),
//                            Text(
//                              'Date:',
//                              textAlign: TextAlign.start,
//                              style: TextStyle(
//                                color: Colors.red,
//                                fontSize: 18.0,
//                                fontWeight: FontWeight.bold,
//                              ),
//                            ),
//                            SizedBox(
//                              width: 10.0,
//                            ),
//                            Text(
//                              'August 4, 2018',
//                              textDirection: TextDirection.ltr,
//                              style: TextStyle(
//                                color: Colors.black,
//                                fontSize: 15.0,
//                                // fontWeight: FontWeight.w900,
//                              ),
//                            ),
//                          ],
//                        ),
//                      ],
//                    ),
//                  )),
//            ),
//          ],
//        ),

      floatingActionButton: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FloatingActionButton(
          onPressed: () {},
          backgroundColor: Colors.red,
          //tooltip: 'Increment',
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(
              Icons.add_rounded,
              color: Colors.white,
              size: 40.0,
            ),
          ),
        ),
      ),
    );
  }
}
