import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';

class Settings extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final String ratingRoute = "/rateApp";
  final String feedbackRoute = "/sendFeedBack";
  final String aboutAppRoute = "/aboutApp";
  final String logOutRoute = "/";
  final privacyPolicyRoute = "/privacyPolicy";
  final paymentMethodRoute = "/bankAccountDetails";

  Widget tapBar({
    @required IconData leadingIcon,
    IconData trailingIcon,
    @required String title,
    String routeName,
  }) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 5.0,
      ),
      child: RaisedButton(
        textColor: Colors.white,
        color: Colors.red,
        child: ListTile(
          contentPadding: EdgeInsets.zero,
          leading: Icon(
            leadingIcon,
            color: Colors.white,
            size: 20.0,
          ),
          trailing: trailingIcon != null
              ? Icon(
                  trailingIcon,
                  color: Colors.white,
                  size: 20.0,
                )
              : Text(""),
          title: Text(
            title,
            style: TextStyle(
                fontSize: 18.0,
                color: Colors.white,
                fontWeight: FontWeight.w400),
          ),
        ),
        onPressed: routeName == null
            ? () {}
            : routeName == logOutRoute
                ? () {
                    logOut();
                  }
                : () {
                    navigationHandler(routeName);
                  },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18.0),
          side: BorderSide(
            color: Colors.red,
          ),
        ),
      ),
    );
  }

  void navigationHandler(String routeName) {
    Navigator.pushNamed(_scaffoldKey.currentState.context, routeName);
  }

  void logOut() {
    Navigator.pushNamedAndRemoveUntil(
        _scaffoldKey.currentState.context, logOutRoute, (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        centerTitle: true,
        leading: Icon(
          Icons.menu,
          color: Colors.white,
        ),
        title: Text('Settings'),
        backgroundColor: Colors.red,
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 15.0,
              ),
//            Container(
//              margin: EdgeInsets.only(left: 10.0, right: 10.0),
//              child: Padding(
//                padding: const EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
//                child: RaisedButton(
//                  textColor: Colors.white,
//                  color: Colors.red,
//                  child: ListTile(
//                    contentPadding: EdgeInsets.zero,
//                    leading: Icon(
//                      Icons.person,
//                      color: Colors.white,
//                      size: 25.0,
//                    ),
//                    title: Text(
//                      'Edit Profile',
//                      style: TextStyle(
//                          fontSize: 18.0,
//                          color: Colors.white,
//                          fontWeight: FontWeight.w400),
//                    ),
//                  ),
//                  onPressed: () {},
//                  shape: RoundedRectangleBorder(
//                    borderRadius: BorderRadius.circular(18.0),
//                    side: BorderSide(
//                      color: Colors.red,
//                    ),
//                  ),
//                ),
//              ),
//            ),,
              tapBar(
                leadingIcon: Icons.person,
                title: "Edit Profile",
              ),
//            Container(
//              margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 2.0),
//              child: Padding(
//                padding: const EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
//                child: RaisedButton(
//                  textColor: Colors.white,
//                  color: Colors.red,
//                  child: ListTile(
//                    contentPadding: EdgeInsets.zero,
//                    leading: Icon(
//                      Icons.credit_card,
//                      color: Colors.white,
//                    ),
//                    title: Text(
//                      'Setup Payments',
//                      style: TextStyle(
//                          fontSize: 18.0,
//                          color: Colors.white,
//                          fontWeight: FontWeight.w400),
//                    ),
//                  ),
//                  onPressed: () {},
//                  shape: RoundedRectangleBorder(
//                    borderRadius: BorderRadius.circular(18.0),
//                    side: BorderSide(
//                      color: Colors.red,
//                    ),
//                  ),
//                ),
//              ),
//            ),
              tapBar(
                leadingIcon: Icons.credit_card,
                title: "Setup Payments",
                routeName: paymentMethodRoute,
              ),

//            Container(
//              margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 2.0),
//              child: Padding(
//                padding: const EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
//                child: RaisedButton(
//                  textColor: Colors.white,
//                  color: Colors.red,
//                  child: ListTile(
//                    contentPadding: EdgeInsets.zero,
//                    leading: Icon(
//                      Icons.star,
//                      color: Colors.white,
//                    ),
//                    title: Text(
//                      'Rate App',
//                      style: TextStyle(
//                          fontSize: 18.0,
//                          color: Colors.white,
//                          fontWeight: FontWeight.w400),
//                    ),
//                  ),
//                  onPressed: () {
//                    Navigator.push(
//                      context,
//                      MaterialPageRoute(
//                        builder: (context) {
//                          return RateApp();
//                        },
//                      ),
//                    );
//                  },
//                  shape: RoundedRectangleBorder(
//                    borderRadius: BorderRadius.circular(18.0),
//                    side: BorderSide(
//                      color: Colors.red,
//                    ),
//                  ),
//                ),
//              ),
//            ),
              tapBar(
                  leadingIcon: Icons.star,
                  title: "Rate App",
                  routeName: ratingRoute),
//            Container(
//              margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 2.0),
//              child: Padding(
//                padding: const EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
//                child: RaisedButton(
//                  textColor: Colors.white,
//                  color: Colors.red,
//                  child: ListTile(
//                    contentPadding: EdgeInsets.zero,
//                    leading: Icon(
//                      Icons.info,
//                      color: Colors.white,
//                    ),
//                    title: Text(
//                      'Send Feedback',
//                      style: TextStyle(
//                          fontSize: 18.0,
//                          color: Colors.white,
//                          fontWeight: FontWeight.w400),
//                    ),
//                  ),
//                  onPressed: () {
//                    Navigator.push(
//                      context,
//                      MaterialPageRoute(
//                        builder: (context) {
//                          return SendFeedback();
//                        },
//                      ),
//                    );
//                  },
//                  shape: RoundedRectangleBorder(
//                    borderRadius: BorderRadius.circular(18.0),
//                    side: BorderSide(
//                      color: Colors.red,
//                    ),
//                  ),
//                ),
//              ),
//            ),
              tapBar(
                  leadingIcon: Icons.info,
                  title: "Send Feedback",
                  routeName: feedbackRoute),

//            Container(
//              margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0),
//              child: Padding(
//                padding: const EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
//                child: RaisedButton(
//                  textColor: Colors.white,
//                  color: Colors.red,
//                  child: ListTile(
//                    contentPadding: EdgeInsets.zero,
//                    trailing: Icon(
//                      Icons.arrow_forward_ios_rounded,
//                      color: Colors.white,
//                      size: 18,
//                    ),
//                    leading: Icon(
//                      Icons.info,
//                      color: Colors.white,
//                    ),
//                    title: Text(
//                      'About the App',
//                      style: TextStyle(
//                          fontSize: 18.0,
//                          color: Colors.white,
//                          fontWeight: FontWeight.w400),
//                    ),
//                  ),
//                  onPressed: () {
//                    Navigator.push(
//                      context,
//                      MaterialPageRoute(
//                        builder: (context) {
//                          return AboutApp();
//                        },
//                      ),
//                    );
//                  },
//                  shape: RoundedRectangleBorder(
//                    borderRadius: BorderRadius.circular(18.0),
//                    side: BorderSide(
//                      color: Colors.red,
//                    ),
//                  ),
//                ),
//              ),
//            ),
              tapBar(
                  leadingIcon: Icons.info,
                  trailingIcon: Icons.arrow_forward_ios_rounded,
                  title: "About The App",
                  routeName: aboutAppRoute),

//            Container(
//              margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 2.0),
//              child: Padding(
//                padding: const EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
//                child: RaisedButton(
//                  textColor: Colors.white,
//                  color: Colors.red,
//                  child: ListTile(
//                    contentPadding: EdgeInsets.zero,
//                    trailing: Icon(
//                      Icons.arrow_forward_ios_rounded,
//                      color: Colors.white,
//                      size: 18,
//                    ),
//                    leading: Icon(
//                      Icons.lock,
//                      color: Colors.white,
//                    ),
//                    title: Text(
//                      'Privacy Policy',
//                      style: TextStyle(
//                          fontSize: 18.0,
//                          color: Colors.white,
//                          fontWeight: FontWeight.w400),
//                    ),
//                  ),
//                  onPressed: () {
//                    Navigator.push(
//                      context,
//                      MaterialPageRoute(
//                        builder: (context) {
//                          return PrivacyPolicy();
//                        },
//                      ),
//                    );
//                  },
//                  shape: RoundedRectangleBorder(
//                    borderRadius: BorderRadius.circular(18.0),
//                    side: BorderSide(
//                      color: Colors.red,
//                    ),
//                  ),
//                ),
//              ),
//            ),
              tapBar(
                  leadingIcon: Icons.lock,
                  trailingIcon: Icons.arrow_forward_ios_rounded,
                  title: "Privacy Policy",
                  routeName: privacyPolicyRoute),

//            Container(
//              margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 2.0),
//              child: Padding(
//                padding: const EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
//                child: RaisedButton(
//                  textColor: Colors.white,
//                  color: Colors.red,
//                  child: ListTile(
//                    contentPadding: EdgeInsets.zero,
//                    trailing: Icon(
//                      Icons.arrow_forward_ios_rounded,
//                      color: Colors.white,
//                      size: 18,
//                    ),
//                    leading: Icon(
//                      FontAwesomeIcons.file,
//                      color: Colors.white,
//                    ),
//                    title: Text(
//                      'Setup Payments',
//                      style: TextStyle(
//                          fontSize: 18.0,
//                          color: Colors.white,
//                          fontWeight: FontWeight.w400),
//                    ),
//                  ),
//                  onPressed: () {},
//                  shape: RoundedRectangleBorder(
//                    borderRadius: BorderRadius.circular(18.0),
//                    side: BorderSide(
//                      color: Colors.red,
//                    ),
//                  ),
//                ),
//              ),
//            ),
              tapBar(
                  leadingIcon: Icons.insert_drive_file,
                  trailingIcon: Icons.arrow_forward_ios_rounded,
                  title: "Terms and Conditions"),

//            Container(
//              margin: EdgeInsets.only(left: 10.0, right: 10.0, top: 2.0),
//              child: Padding(
//                padding: const EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
//                child: RaisedButton(
//                  textColor: Colors.white,
//                  color: Colors.red,
//                  child: ListTile(
//                    contentPadding: EdgeInsets.zero,
//                    leading: Icon(
//                      Icons.logout,
//                      color: Colors.white,
//                    ),
//                    title: Text(
//                      'Log out',
//                      style: TextStyle(
//                          fontSize: 18.0,
//                          color: Colors.white,
//                          fontWeight: FontWeight.w400),
//                    ),
//                  ),
//                  onPressed: () {
//                    Navigator.of(context).pushAndRemoveUntil(
//                        MaterialPageRoute(builder: (context) => MyApp()),
//                        (route) => false);
//                  },
//                  shape: RoundedRectangleBorder(
//                    borderRadius: BorderRadius.circular(18.0),
//                    side: BorderSide(
//                      color: Colors.red,
//                    ),
//                  ),
//                ),
//              ),
//            ),
              tapBar(
                  leadingIcon: Icons.logout,
                  title: "Log Out",
                  routeName: logOutRoute),
            ],
          ),
        ),
      ),
    );
  }
}
