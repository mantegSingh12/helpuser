import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:myfirst_app/screens/bankAccountDetails.dart';
import 'package:myfirst_app/screens/editProfile.dart';
import 'package:myfirst_app/screens/profile.dart';

import 'package:myfirst_app/screens/signup.dart';
import 'package:myfirst_app/widgets/constants.dart';
import 'package:myfirst_app/screens/home.dart';
import 'package:myfirst_app/screens/aboutapp.dart';
import 'package:myfirst_app/screens/helper.dart';
import 'package:myfirst_app/screens/myposts.dart';
import 'package:myfirst_app/screens/newrequest.dart';
import 'package:myfirst_app/screens/notifications.dart';
import 'package:myfirst_app/screens/privacypolicy.dart';
import 'package:myfirst_app/screens/rateapp.dart';
import 'package:myfirst_app/screens/sendfeedback.dart';
import 'package:myfirst_app/screens/settings.dart';

enum Choice {
  user,
  server,
}

void main() {
  runApp(
    MaterialApp(
      //home: MyApp(),
      initialRoute: "/",
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        "/": (context) => MyApp(),
        "/signUp": (context) => SignUp(),
        "/home": (context) => Home(),
        "/bankAccountDetails": (context) => BankAccountDetails(),
        "/profile": (context) => Profile(),
        "/editProfile": (context) => EditProfile(),
        "/aboutApp": (context) => AboutApp(),
        "/helper": (context) => Helper(),
        "/myPosts": (context) => MyPosts(),
        "/newRequest": (context) => NewRequest(),
        "/notifications": (context) => Notifications(),
        "/privacyPolicy": (context) => PrivacyPolicy(),
        "/rateApp": (context) => RateApp(),
        "/sendFeedBack": (context) => SendFeedback(),
        "/settings": (context) => Settings(),
      },
    ),
  );
}

class MyApp extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<MyApp> {
  TextEditingController nameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  Choice selectedChoice;
  String homeRoute = "/home";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Container(
              // height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                // crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  SizedBox(
                    height: 35.0,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12.0),
                        color: Color(0xffff2a34),
                        border: Border.all(color: Colors.black, width: 4.0)),
                    height: 100,
                    width: 100,
                    child: Center(
                        child: Text(
                      'HELPER',
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 18.0),
                    )),
                  ),
                  SizedBox(
                    height: 40.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Text(
                        'Sign up as a...',
                        style: TextStyle(
                            color: Colors.grey.shade400,
                            fontSize: 18,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                        height: 20.0,
                        minWidth: 160.0,
                        onPressed: () {
                          setState(() {
                            selectedChoice = Choice.user;
                          });
                        },
                        padding: EdgeInsets.symmetric(
                            horizontal: 15.0, vertical: 10.0),
                        color: selectedChoice == Choice.user
                            ? kActiveCardColour
                            : kInactiveCardColour,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Text(
                          'User',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: selectedChoice == Choice.user
                                ? kActiveTextColour
                                : kInactiveTextColour,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      FlatButton(
                        height: 20.0,
                        minWidth: 160.0,
                        onPressed: () {
                          setState(() {
                            selectedChoice = Choice.server;
                          });
                        },
                        padding: EdgeInsets.symmetric(
                            horizontal: 15.0, vertical: 10.0),
                        color: selectedChoice == Choice.server
                            ? kActiveCardColour
                            : kInactiveCardColour,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Text(
                          'Helper',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            // fontFamily: 'BalsamiqSans',
                            color: selectedChoice == Choice.server
                                ? Colors.grey.shade400
                                : kInactiveTextColour,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Divider(
                    color: Colors.grey[350],
                    height: 25,
                    thickness: 2,
                    indent: 5,
                    endIndent: 5,
                  ),
                  Container(
                    margin:
                        EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                    height: MediaQuery.of(context).size.height * 0.07,
                    width: MediaQuery.of(context).size.width,
                    child: TextFormField(
                      // textAlignVertical: TextAlignVertical.center,
                      textAlign: TextAlign.center,

                      controller: nameController,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 15.0, vertical: 10),
                        filled: true,
                        fillColor: Colors.grey.shade200,
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.grey.shade200,
                            width: 2.0,
                          ),
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.grey.shade100,
                            width: 2.0,
                          ),
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        hintText: 'Email',
                        hintStyle: TextStyle(
                          color: Colors.grey.shade400,
                          fontSize: 18.0,
                          // fontFamily: 'BalsamiqSans',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin:
                        EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                    height: MediaQuery.of(context).size.height * 0.07,
                    width: MediaQuery.of(context).size.width,
                    child: TextFormField(
                      // textAlignVertical: TextAlignVertical.center,
                      textAlign: TextAlign.center,

                      controller: nameController,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 15.0, vertical: 10),
                        filled: true,
                        fillColor: Colors.grey.shade200,
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.grey.shade200,
                            width: 2.0,
                          ),
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                            color: Colors.grey.shade100,
                            width: 2.0,
                          ),
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        hintText: 'Password',
                        hintStyle: TextStyle(
                          color: Colors.grey.shade400,
                          fontSize: 18.0,
                          // fontFamily: 'BalsamiqSans',
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width: MediaQuery.of(context).size.width,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(
                            color: Colors.red[500],
                          ),
                        ),
                        // padding: EdgeInsets.all(18.0),
                        textColor: Colors.white,
                        color: Color(0xffff2a34),
                        child: Text(
                          'Let\'s Go!',
                          style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 16.0,
                          ),
                        ),
                        onPressed: () {
                          Navigator.pushReplacementNamed(context, homeRoute);
                        },
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 5.0,
                      left: 20.0,
                      right: 20.0,
                    ),
                    child: Text(
                      'Forgot Password?',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.grey.shade400,
                        fontWeight: FontWeight.w500,
                        fontSize: 16.0,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: 60,
                          width: 60,
                          child: SvgPicture.asset(
                            'images/facebook.svg',
                            height: 15,
                            width: 15,
                          ),
                          decoration: BoxDecoration(
                            color: Color(0xff42609c),
                            shape: BoxShape.circle,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 30),
                          height: 60,
                          width: 60,
                          child: SvgPicture.asset(
                            'images/google-plus.svg',
                            height: 15,
                            width: 15,
                          ),
                          decoration: BoxDecoration(
                            color: Color(0xff42609c),
                            shape: BoxShape.circle,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      left: 15.0,
                      right: 15.0,
                      top: 10.0,
                      bottom: 15.0,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Not Registered?',
                          style: TextStyle(
                            color: Colors.grey.shade400,
                            fontSize: 15.0,
                          ),
                        ),
                        GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignUp())),
                          child: Container(
                            margin: EdgeInsets.only(left: 10.0),
                            child: Text(
                              'Join up!',
                              style: TextStyle(
                                fontSize: 15.0,
                                color: Colors.red[500],
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
