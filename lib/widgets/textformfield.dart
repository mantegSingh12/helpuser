import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:myfirst_app/widgets/responsive_ui.dart';

class CustomTextField extends StatelessWidget {
  Function validator;
  Function onFieldSubmitted;
  final Key key;
  final String hint;
  final TextEditingController textEditingController;
  final TextInputType keyboardType;
  final bool obscureText;
  final IconData icon;
  double _width;
  double _pixelRatio;
  bool large;
  bool medium;

  CustomTextField({
    this.key,
    this.validator,
    this.onFieldSubmitted,
    this.hint,
    this.textEditingController,
    this.keyboardType,
    this.icon,
    this.obscureText = false,
  });

  @override
  Widget build(BuildContext context) {
    _width = MediaQuery.of(context).size.width;
    _pixelRatio = MediaQuery.of(context).devicePixelRatio;
    large = ResponsiveWidget.isScreenLarge(_width, _pixelRatio);
    medium = ResponsiveWidget.isScreenMedium(_width, _pixelRatio);
    return
//      Material(
//      borderRadius: BorderRadius.circular(30.0),
//      elevation: large ? 12 : (medium ? 10 : 8),
//      child:
        TextFormField(
      key: key,
      validator: validator,
      onFieldSubmitted: onFieldSubmitted,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      controller: textEditingController,
      keyboardType: keyboardType,
      cursorColor: Colors.grey,
      decoration: InputDecoration(
        filled: true,
        fillColor: Colors.grey[350],
        prefixIcon: Icon(icon, color: Colors.grey, size: 20),
        hintText: hint,
        hintStyle: TextStyle(
          fontFamily: 'BalsamiqSans',
          fontSize: 15.0,
          fontWeight: FontWeight.bold,
          color: Colors.grey,
        ),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30.0),
            borderSide: BorderSide.none),
      ),
    );
    // );
  }
}
